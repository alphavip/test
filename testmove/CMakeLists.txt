cmake_minimum_required(VERSION 2.8)

project(move)

aux_source_directory(. Files)
add_executable(move ${Files})