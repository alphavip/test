#include <iostream>

#include <memory>

#include <vector>

class Role
{
public:
    Role()
    {
        std::cout << "constructor:" << std::endl;
    }
    Role(int id, const std::string& name):_id(id), _name(std::move(name))
    {
        std::cout << "constructor:" << _name.c_str() << std::endl;
    }
    Role(const Role& role)
    {
        this->_id = role._id;
        this->_name = role._name;
        std::cout << "copy constructor:" << _name.c_str() << std::endl;
    }
    Role(Role&& role):_id(role._id), _name(std::move(role._name))
    {
        std::cout << "move constructor:" << _name.c_str() << std::endl;
    }
    
    virtual ~Role()
    {
        std::cout << "desdroy:" << _id << " " << _name.c_str() << std::endl;
    }
    
private:
    int _id;
    std::string _name;
};

Role CreateRole(int id, const std::string& name)
{
    Role role(id, name);
    return role;
}

int main()
{
    std::vector<Role> roles;
    roles.push_back(Role(1, "alpha"));
    Role beta(2, "beta");
    roles.push_back(beta);
    
    Role gama = CreateRole(3, "gama");

    roles.push_back(gama);    
    return 0;
}
